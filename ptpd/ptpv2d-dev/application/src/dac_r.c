// DAC_R.c
// This is the R filter(the first stage) proposed in
// Delay Asymmetry Correction Model for IEEE 1588 Synchronization protocol
// written by Arifur Rahman.

// author: #parhat.

// alpha, beta, gamma. #paraht.

#include "ptpd.h"
#include <limits.h>
#include "datatypes.h"

// NOTE: the section below can be replaced by msd and smd directly. #parhat.
// // funtion to calc (t2-t1) for later use in gamma. #parhat.
// void alpha(*x, *y)
// {
//   if((*x != 0) && (*y != 0))
//   {
//     subTime(alpha, &ptpClock->t2_sync_rx_time, &ptpClock->t1_sync_tx_time);
//     printf(" alpha(T2-T1) calculated.\n");
//     return alpha;
//   }
//   else{
//     printf(" something went wrong in alpha.\n");
//   }
// }
//
// // function to calc (t4-t3) for later use in gamma. #parhat.
// void beta(*x, *y)
// {
//   if((*x !=0) && (*y !=0))
//   {
//     subTime(beta, &ptpClock->t4_delay_req_rx_time, &ptpClock->t3_delay_req_tx_time);
//     printf(" beta(T4-T3) calculated.\n");
//     return beta;
//   }
//   else{
//     printf(" something went wrong in beta.\n");
//   }
// }

// function to determin the R ratio in DAC model. #parhat.
// NOTE: changed void MSD_SMD to double MSD_SMD both in here and in ptpd.h

// double R = 0.0;
double MSD_SMD(PtpClock *ptpClock)
{
  unsigned long int MSD;
  unsigned long int SMD;
  double MSD_d;
  double SMD_d;
  double R = 0.0;
  // change second to nanosecond,
  // and add it to nanosecond for easy calculation;
  MSD = (ptpClock->master_to_slave_delay.seconds * 1000000000ULL
      + ptpClock->master_to_slave_delay.nanoseconds);
  SMD = (ptpClock->slave_to_master_delay.seconds * 1000000000ULL
      + ptpClock->slave_to_master_delay.nanoseconds);
// printf("\nMSD: %10dns; SMD: %10dns.\n", MSD, SMD);

  // change MSD and SMD to double value for division.
  MSD_d = ((double)abs(MSD)) * 1e-4;
  SMD_d = ((double)abs(SMD)) * 1e-4;
  R = MSD_d / SMD_d;
  // printf("MSD_d = %.1f, SMD_d = %.1f, ", MSD_d, SMD_d);
  // printf("R(MSD/SMD) = %.5f\n", R);
  return R;
}

// function filter.
// void R_filt(double ratio, RunTimeOpts *rtOpts, PtpClock *ptpClock)
// {
//   // if this is the first time of receiving sync message, just return true.
//   if(!ptpClock->parent_last_sync_sequence_number)
//   {
//     return;
//   }
//   else if(ratio <= 1.2 && ratio >= 0.8)
//   {
//     // ratio less than or equal to 1.2 and greater than or equal to 0.8,
//     // is in the desired area, return true.
//     return;
//   }
//   else if(ratio < 0.8)
//   {
//     // ratio less than 0.8, means MSD is smaller than SMD,
//     // possible delay_request or delay_response encountered network gitter,
//     // re-send delay request.
//
//     issueDelayReq(rtOpts, ptpClock);
//   }
//   else if(ratio > 1.2)
//   {
//     // ratio greater than 1.2, means MSD is greater than SMD,
//     // possible Sync or follow_up packet encountered network gitter,
//     // set all to null then wait for another Sync packet.
//
//     // NOTE: currently just return.
//     return;
//   }
// }

// NOTE: trying to integrate the filter right into protocol.c.
// rather than writing a function for it.
// it is more flexible to do so.

/*
##########################################################################parhat
*/
        // NOTE: add the R_filt right here, after updateOffset, before updateClock. #parhat.
        // R_filt(MSD_SMD(ptpClock), rtOpts, ptpClock);
        // NOTE: section below causing problem, commented out temp.
        // if(!ptpClock->parent_last_sync_sequence_number)
        // {
        //   printf("handleSync: first sync, updating clock without R_filt.\n");
        //   updateClock(rtOpts, ptpClock);
        // }
        // else if(MSD_SMD(ptpClock) != 0)
        // {
          // if(MSD_SMD(ptpClock) >= 0.8 && MSD_SMD(ptpClock) <= 1.2)
          // {
          //   // MSD_SMD testing success, update clock as normal.
          //   printf("R_filt normal, updating clock.\n");
          //   updateClock(rtOpts, ptpClock);
          // }
          // else if(MSD_SMD(ptpClock) < 0.8)
          // {
          //   // ratio less than 0.8, MSD smaller than SMD.
          //   // possible delay_request or delay_response encountered jitter.
          //   // here in handleSync, this is irrelevent, do nothing.
          //   printf("handleSync: MSD_SMD less than 0.8, do nothing.\n");
          // }
          // else if(MSD_SMD(ptpClock) > 1.2)
          // {
          //   // ratio greater than 1.2, MSD greater than SMD.
          //   // possible Sync or Followup message encountered jitter.
          //   // don't update the clock with current values,
          //   // reset and wait for another sync.
          //   printf("R_filt: MSD_SMD greater than 1.2\n");
          //   // clearTime(&ptpClock->t1_sync_tx_time);
          //   // clearTime(&ptpClock->t2_sync_rx_time);
          //   // NOTE: cannot call initClockVars here, will cause problems.
          //   // printf("Called initClockVars.\n");
          //   // initClockVars(rtOpts, ptpClock);
          //   // NOTE: currently updating clock for testing.
          //   updateClock(rtOpts, ptpClock);
          // }
        // }
        // else
        // {
        //   printf("protocol.c: updating clock without R_filt\n");
        //   updateClock(rtOpts, ptpClock);
        // }
/*
##########################################################################parhat
*/
// NOTE: add R_filt here after updateOffset, before updateClock. #parhat.
// R_filt(MSD_SMD(ptpClock), rtOpts, ptpClock);
// if(MSD_SMD(ptpClock) <= 1.2 && MSD_SMD(ptpClock) >= 0.8)
// {
//   updateClock(rtOpts, ptpClock);
// }
// NOTE: updateClock moved into the if statement above.

void DAC_updateClock(RunTimeOpts *rtOpts, PtpClock *ptpClock)
{
  Integer32    adj=0;
  TimeInternal timeTmpA;  // AKB: Added values for adjusting calc based on time to get time
  TimeInternal timeTmpB;
  TimeInternal timeTmpC;
  TimeInternal timeTmpD;
  TimeInternal timeTmpE;
  TimeInternal timeTmpF;
  Integer64    delta_time_calc;

  DBGV("DAC_updateClock:\n");

  if(ptpClock->offset_from_master.seconds)
  {
    /* if offset from master seconds is non-zero, then this is a "big jump:
     * in time.  Check Run Time options to see if we will reset the clock or
     * set frequency adjustment to max to adjust the time
     */
    if(!rtOpts->noAdjust)
    {
      if(!rtOpts->noResetClock)
      {

        if (!isNonZeroTime(&ptpClock->t1_sync_delta_time))
        {
           // Delta time is zero, so this is the first sync to capture and we'll do the major
           // adjustment on the next sync instead of this one
           //
           // Store t1 and t2 times as current delta, next time we'll subtract
           //
           copyTime(&ptpClock->t1_sync_delta_time,
                    &ptpClock->t1_sync_tx_time
                   );
           copyTime(&ptpClock->t2_sync_delta_time,
                    &ptpClock->t2_sync_rx_time
                   );
           NOTIFY("DAC_updateClock: Storing current T1 and T2 values for later calc\n");
           DBG("DAC_updateClock: Storing T1: %10ds %11dns\n",
               ptpClock->t1_sync_delta_time.seconds,
               ptpClock->t1_sync_delta_time.nanoseconds
              );
           DBG("DAC_updateClock: Storing T2: %10ds %11dns\n",
               ptpClock->t2_sync_delta_time.seconds,
               ptpClock->t2_sync_delta_time.nanoseconds
              );
           return;
        }

        // If we are here then t1 and t2 sync delta were set to previous t1 and t2
        // values.  Now we calculate the deltas

           DBG("DAC_updateClock: Current T1: %10ds %11dns\n",
               ptpClock->t1_sync_tx_time.seconds,
               ptpClock->t1_sync_tx_time.nanoseconds
              );
           DBG("DAC_updateClock: Current T2: %10ds %11dns\n",
               ptpClock->t2_sync_rx_time.seconds,
               ptpClock->t2_sync_rx_time.nanoseconds
              );

        subTime(&ptpClock->t1_sync_delta_time,
                &ptpClock->t1_sync_tx_time,
                &ptpClock->t1_sync_delta_time
               );
        subTime(&ptpClock->t2_sync_delta_time,
                &ptpClock->t2_sync_rx_time,
                &ptpClock->t2_sync_delta_time
               );

           DBG("DAC_updateClock: Delta   T1: %10ds %11dns\n",
               ptpClock->t1_sync_delta_time.seconds,
               ptpClock->t1_sync_delta_time.nanoseconds
              );
           DBG("DAC_updateClock: Delta   T2: %10ds %11dns\n",
               ptpClock->t2_sync_delta_time.seconds,
               ptpClock->t2_sync_delta_time.nanoseconds
              );

        // Now we get the difference between the two time bases and store in the T2 time delta
        // as we will use the T1 time as the divisor (so master clock drives the time)

        subTime(&ptpClock->t2_sync_delta_time,
                &ptpClock->t2_sync_delta_time,
                &ptpClock->t1_sync_delta_time
               );

           DBG("DAC_updateClock: Delta T2 - Delta T1: %10ds %11dns\n",
               ptpClock->t2_sync_delta_time.seconds,
               ptpClock->t2_sync_delta_time.nanoseconds
              );

        delta_time_calc =  getNanoseconds(&ptpClock->t2_sync_delta_time)
                           * 1000000000;
        delta_time_calc /= getNanoseconds(&ptpClock->t1_sync_delta_time);

           DBG("DAC_updateClock: Calculated Parts/billion: %d\n",
               (int)delta_time_calc
              );


        /* clamp the accumulator to ADJ_FREQ_MAX for sanity */
        if(     delta_time_calc > ADJ_FREQ_MAX)
          adj =  ADJ_FREQ_MAX;
        else if(delta_time_calc < -ADJ_FREQ_MAX)
          adj = -ADJ_FREQ_MAX;
        else
          adj = (UInteger32)delta_time_calc;

        NOTIFY("DAC_updateClock: Initial clock adjust: %d, base: %d\n",
                adj,
                ptpClock->baseAdjustValue
              );

        NOTIFY("DAC_updateClock: Offset from Master %ds.%9.9d seconds\n",
                ptpClock->offset_from_master.seconds,
                ptpClock->offset_from_master.nanoseconds
              );
        DBG( "DAC_updateClock: offset_from_master seconds != 0\n");
        DBGV("  master-to-slave delay:   %10ds %11dns\n",
             ptpClock->master_to_slave_delay.seconds,
             ptpClock->master_to_slave_delay.nanoseconds
            );
        DBGV("  slave-to-master delay:   %10ds %11dns\n",
             ptpClock->slave_to_master_delay.seconds,
             ptpClock->slave_to_master_delay.nanoseconds
            );
        DBGV("  one-way delay:           %10ds %11dns\n",
             ptpClock->one_way_delay.seconds,
             ptpClock->one_way_delay.nanoseconds
            );
        DBG( "  offset from master:      %10ds %11dns\n",
             ptpClock->offset_from_master.seconds,
             ptpClock->offset_from_master.nanoseconds
           );
        DBG( "  observed drift:          %10d\n",
            ptpClock->observed_drift
           );

        getTime(&timeTmpA, ptpClock->current_utc_offset);   // Get current time #1

        getTime(&timeTmpB, ptpClock->current_utc_offset);   // Get current time #2

        subTime(&timeTmpC,    // Calculate time   #3, time elapsed between calls
                &timeTmpB,
                &timeTmpA
               );

        getTime(&timeTmpD, ptpClock->current_utc_offset);   // Get current time #4

        subTime(&timeTmpE,    // Subtract calculated offset from master
                &timeTmpD,
                &ptpClock->saved_offset_from_master
               );

        addTime(&timeTmpF,    // Add calculated time to get timer value
                &timeTmpE,
                &timeTmpC
               );

        setTime(&timeTmpF, ptpClock->current_utc_offset);   // Set new PTP time

        DBGV(" get  Time A           :   %10ds %11dns\n",
             timeTmpA.seconds,
             timeTmpA.nanoseconds
            );
        DBGV(" get  Time B           :   %10ds %11dns\n",
             timeTmpB.seconds,
             timeTmpB.nanoseconds
            );
        DBGV(" calc Time C (B-A)     :   %10ds %11dns\n",
             timeTmpC.seconds,
             timeTmpC.nanoseconds
            );
        DBGV(" get  Time D           :   %10ds %11dns\n",
             timeTmpD.seconds,
             timeTmpD.nanoseconds
            );
        DBGV(" offset from master    :   %10ds %11dns\n",
             ptpClock->offset_from_master.seconds,
             ptpClock->offset_from_master.nanoseconds
            );
        DBGV(" calc Time E (D+offset):   %10ds %11dns\n",
             timeTmpE.seconds,
             timeTmpE.nanoseconds
            );
        DBGV(" calc Time F (E+C)     :   %10ds %11dns\n",
             timeTmpF.seconds,
             timeTmpF.nanoseconds
            );
        DBGV("DAC_updateClock: set time to Time F\n");

        // Initialize clock variables based on run time options (rtOpts)

        // initClockVars(rtOpts, ptpClock);
        // NOTE: something wrong in here, copying the content of initClockVars
        // below.
        /* clear vars */
        clearTime(&ptpClock->master_to_slave_delay);
        clearTime(&ptpClock->slave_to_master_delay);
        clearTime(&ptpClock->offset_from_master);   /* AKB: 9/18/2007 Clear offset from master */
        ptpClock->ofm_filt.y                        = 0;
        ptpClock->ofm_filt.nsec_prev                = -1; /* AKB: -1 used for non-valid nsec time */

        ptpClock->observed_v1_variance = 0;
        ptpClock->observed_drift       = 0;  /* clears clock servo accumulator (the I term) */
        ptpClock->owd_filt.s_exp       = 0;  /* clears one-way delay filter */
        ptpClock->halfEpoch            = ptpClock->halfEpoch || rtOpts->halfEpoch;
        rtOpts->halfEpoch              = 0;

        // Adjust clock based on calculation from Delta T1, T2 times

        adjFreq(ptpClock->baseAdjustValue - adj);

        // Set initial observed drift to this calculated value

        ptpClock->observed_drift = adj;

        DBG( "DAC_updateClock: after initClock:\n");
        DBGV("  master-to-slave delay:   %10ds %11dns\n",
             ptpClock->master_to_slave_delay.seconds,
             ptpClock->master_to_slave_delay.nanoseconds
            );
        DBGV("  slave-to-master delay:   %10ds %11dns\n",
             ptpClock->slave_to_master_delay.seconds,
             ptpClock->slave_to_master_delay.nanoseconds
            );
        DBG( "  one-way delay:           %10ds %11dns\n",
             ptpClock->one_way_delay.seconds,
             ptpClock->one_way_delay.nanoseconds
           );
        DBG( "  offset from master:      %10ds %11dns\n",
             ptpClock->offset_from_master.seconds,
             ptpClock->offset_from_master.nanoseconds
           );
        DBG( "  observed drift:          %10d\n",
             ptpClock->observed_drift
           );

      }
      else
      {
        /* Run time options indicate we can't reset the clock, so we slow
         * it down or speed it up based on ADJ_FREQ_MAX adjustment rather
         * than actually setting the time.
         */
        adj = ptpClock->offset_from_master.nanoseconds > 0 ? ADJ_FREQ_MAX : -ADJ_FREQ_MAX;
        adjFreq(ptpClock->baseAdjustValue - adj);
      }
    }
  }
  else
  {
    /* Offset from master is less than one second.  Use the the PI controller
     * to adjust the time
     */
// NOTE: commented out for plotting data.
    // printf("DAC_updateClock: using PI controller to update clock\n");

    /* no negative or zero attenuation */
    if(rtOpts->ap < 1)
     rtOpts->ap = 1;
    if(rtOpts->ai < 1)
      rtOpts->ai = 1;

    DBGV("  previous observed drift: %10d\n",
         ptpClock->observed_drift
        );
    DBGV("  run time opts P:         %10d\n",
         rtOpts->ap
        );
    DBGV("  run time opts I:         %10d\n",
         rtOpts->ai
        );

    DBGV("  current observed drift:  %d\n",
         ptpClock->observed_drift
        );


    DBGV("  current offset           %dns\n",
         rtOpts->ai
        );

    /* the accumulator for the I component */
    ptpClock->observed_drift += ptpClock->offset_from_master.nanoseconds/rtOpts->ai;

    DBGV("  new observed drift (I):  %d\n",
         ptpClock->observed_drift
        );

    /* clamp the accumulator to ADJ_FREQ_MAX for sanity */
    if(     ptpClock->observed_drift > ADJ_FREQ_MAX)
      ptpClock->observed_drift =  ADJ_FREQ_MAX;
    else if(ptpClock->observed_drift < -ADJ_FREQ_MAX)
      ptpClock->observed_drift = -ADJ_FREQ_MAX;

    DBGV("  clamped drift:           %d\n",
         ptpClock->observed_drift
        );

    adj = ptpClock->offset_from_master.nanoseconds/rtOpts->ap + ptpClock->observed_drift;

    DBGV("  calculated adjust:       %d\n",
         adj
        );

    DBGV("  base adjust:             %d\n",
         ptpClock->baseAdjustValue
        );

    /* apply controller output as a clock tick rate adjustment */
    if(!rtOpts->noAdjust)
    {
      DBGV("  calling adjFreq with:    %d\n",
           (ptpClock->baseAdjustValue-adj)
          );

      adjFreq(ptpClock->baseAdjustValue - adj);
      if (rtOpts->rememberAdjustValue == TRUE)
      {
         if (   ptpClock->offset_from_master.nanoseconds <= 100
             && ptpClock->offset_from_master.nanoseconds >= -100
            )
         {
           ptpClock->lastAdjustValue = -adj;  // Store value if it gave a good clock
                                              // result.
         }
      }
    }
  }

  if(rtOpts->displayStats)
  {
    displayStats(rtOpts, ptpClock);
  }
}
